package Animal;
import Attraction.*;
public abstract class Animal implements Attraction{
    private String nama;
    private String type;
    private int id;
    private double length;
    private double weight;
    private String gender;
    private String specialStatus;
    private boolean health;

    public Animal(String nama, int id, double length, double weight, String gender, String type, String health) {
        this.nama = nama;
        this.length = length;     
        this.type = type;
        this.weight = weight;
        this.gender = gender;
        this.id = id;
        if(health.equals("healthy")) this.health = true;
        else this.health = false;
        this.specialStatus = "";
    } 
    public Animal(String nama, int id, double length, double weight, String gender, String type, String specialStatus, String health) {
        this.nama = nama;
        this.length = length;     
        this.type = type;
        this.weight = weight;
        this.gender = gender;
        this.id = id;
        if(health.equals("healthy")) this.health = true;
        else this.health = false;
        this.specialStatus = specialStatus;
    }
    
    public boolean circleOfFire() {
        return false;
    }
    public boolean countingMaster() {
        return false;
    }
    public boolean dancingAnimal() {
        return true;
    }
    public boolean passionateCoders() {
        return false;
    }
    public String getNama() {
        return this.nama;
    }
    public double getLength() {
        return this.length;
    }
    public int getId(){
        return this.id;
    }
    public double getWeight() {
        return this.weight;
    }
    public String getGender() {
        return this.gender;
    }   
    public String getType(){
        return this.type;
    }
    public boolean getHealth() {
        return this.health;
    }
    public String getSpecialStatus() {
        return this.specialStatus;
    }
}