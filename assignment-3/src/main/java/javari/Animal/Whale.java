package Animal;
import Animal.Animal;
public class Whale extends Mammal {
    private String specialStatus;
    
    public Whale(String nama, int id, double length, double weight, String gender, String type, String health){
        super(nama, id, length, weight, gender, type, health);
        this.specialStatus = "";
    }
    public Whale(String nama, int id, double length, double weight, String gender, String type,String specialStatus, String health){
        super(nama, id, length, weight, gender, type, specialStatus, health);
    }
    public boolean circleOfFire() {
        if(this.getHealth()==true && this.getSpecialStatus().equalsIgnoreCase("")) return true;
        else return false;
    }
    public boolean countingMaster() {
        if(this.getHealth()==true && this.getSpecialStatus().equalsIgnoreCase("")) return true;
        else return false;
    }
}