package Regis;
import java.util.List;
import java.util.ArrayList;
import Animal.Animal;
import Attraction.SelectedAttraction;
public class Regis{ //Class Regis yang berarti "Objek" pendaftar dan berujung membaut "objek" ticket
    private int id;
    private String nama;
    private List<SelectedAttraction> selected;
    public Regis(int id, String nama) {
        this.id = id;
        this.nama = nama;
        this.selected = new ArrayList<SelectedAttraction>();
    }
    public int getRegistrationId() {
        return this.id;
    }
    public String getVisitorName() {
        return this.nama;
    }
    public String setVisitorName(String name) {
        this.nama = name;
        return this.nama;
    }
    public List<SelectedAttraction> getSelectedAttractions() {
        return this.selected;
    }
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        if(selected instanceof SelectedAttraction){
            this.selected.add(selected);
            return true;
        }
        return false;
    }
}
