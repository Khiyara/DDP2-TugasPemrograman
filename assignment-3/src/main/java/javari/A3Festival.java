import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import Animal.*;
import Regis.*;
import Attraction.*;

public class A3Festival {
    // Attribute buat mencek validasi dan arrayList hewan
    static int error = 0;
    static int valid = 0;
    static int errorSection = 0;
    static int id = 1;
    static String input;
    static boolean[][] listAttraction;
    static boolean[] correctSection;
    static boolean[] adaAnimals;
    static boolean[] canDoAttraction;
    static ArrayList<Regis> listRegis = new ArrayList<Regis>();
    static Scanner in = new Scanner(System.in);
    static ArrayList<ArrayList<Animal>> animalList = new ArrayList<ArrayList<Animal>>();
    public static void main (String [] args) { 
    //Array index menggunakan jenis hewan agar mudah mengelompokkan
        int cat = 0; int hamster = 1; int snake = 6; int eagle = 4; int parrot = 5; int whale = 3;int lion = 2;
        int cof = 0; int da = 1; int cm = 2; int pc = 3;
        for(int i=0;i<7;i++) animalList.add(new ArrayList<Animal>());
        System.out.println("Welcome to Javari Park Festival - Registration Service!");
        System.out.print("... Opening default section database from data. ... File not found or incorrect file! \nPlease provide the source data path: ");
        input = in.nextLine();
        adaAnimals = new boolean[7];
        listAttraction = new boolean[7][4];
        canDoAttraction = new boolean[7];
        correctSection = new boolean[3];
        File file = new File(input+"animals_records.csv");
        File file1 = new File(input+"animals_attractions.csv");
        File file2 = new File(input+"animals_categories.csv");
        CSVReader.readAnimalRecord(file);
        CSVReader.readAnimalAttraction(file1);
        CSVReader.readAnimalSection(file2);
        int correctAtt = 0;        
        int correctCateg = 0;
        boolean[] correctAttraction = new boolean[4];
        for (int i=0; i < listAttraction.length; i++){
            for (int z =0; z < listAttraction[i].length; z++){
                if(listAttraction[i][z]) {
                    correctAttraction[z] = true;
                }
            }
        }
        for (int i=0; i < correctAttraction.length; i++) if (correctAttraction[i]) correctAtt++;  
        int correctSect = 0;
        for (boolean x : correctSection) if (x) {
            correctSect++;
            correctCateg++;
        }
        System.out.println("... Loading... Success... System is populating data...");
        System.out.println("Found _"+correctSect+"_ valid sections and _"+(3-correctSect)+"_ invalid sections");
        System.out.println("Found _"+correctAtt+"_ valid attractions and _"+(4-correctAtt)+"_ invalid attractions");
        System.out.println("Found _"+(correctCateg)+"_ valid animal categories and _"+(3-correctCateg)+"_ invalid animal categories");
        System.out.println("Found _"+(valid)+"_ valid animal records and _"+error+"_ invalid animal records");
        System.out.println("Welcome to Javari Park Festival - Registration Service!\nPlease answer the questions by typing the number. Type # if you want to return to the previous menu");
        String textAsk = "Javari Park has "+correctSect+" sections\n";
        int count = 1;
        for (int i=0;i<correctSection.length;i++){ //String generator
            if (i==0) {
            textAsk += (count)+". Explore the Mammals\n"; count++;}
            else if (i==1) {
            textAsk += (count)+". World of Aves\n"; count++;}
            else if (i==2) {
            textAsk += (count)+". Reptilian Kingdom\n";count++;}
        }
        count = 1;
        String textAnimalMammal = "";
        for (int i=0;i<animalList.size()-3;i++) {
            textAnimalMammal += count+". "+animalList.get(i).get(0).getType()+"\n";
            count++;
        }
        count = 1;
        String textAnimalAves = "";
        for (int i=4;i<animalList.size()-1;i++) {
            textAnimalAves += count+". "+animalList.get(i).get(0).getType()+"\n";
            count++;
        }
        String textAnimalReptil = "1. "+animalList.get(6).get(0).getType();
        //Selama masih ingin buat ticket maka masih berjalan program, "Break" ketika hanya selesai membuat ticket
        while(true) {
            System.out.println(textAsk);
            System.out.print("Please choose your preferred section (type the number): ");
            String ask = in.next();
            try {
                int command = Integer.parseInt(ask);
                if (command==1) {
                    if (!correctSection[0]) {
                        System.out.println("Unfortunately, there is no Explore the Mammals recorded, please choose other sections");
                        continue;
                    }
                    System.out.println(textAnimalMammal);
                    System.out.print("Please choose your preferred animals (type the number): ");
                    String secAsk = in.next();
                    if (isNumeric(secAsk)) {
                        int secCommand = Integer.parseInt(secAsk);
                        if (secCommand == 1) {
                            if(canDoAttraction[cat] && cekAttraction(animalList.get(cat))) {
                                System.out.print(animalAttraction("Cat"));
                                String thirdAsk = in.next();
                                if(isNumeric(thirdAsk)) {
                                    int thirdCommand = Integer.parseInt(thirdAsk);
                                    if(thirdCommand==1){
                                        if (listAttraction[cat][da]){
                                            System.out.print("One more step, please let us know your name: ");
                                            in.nextLine();
                                            String namaVisitor = in.nextLine();
                                            RegisGenerator.attractionGenerator("Dancing Animals", animalList.get(cat), namaVisitor);
                                            String ans = in.next();
                                            if (ans.equalsIgnoreCase("y")) {
                                                RegisGenerator.regisGenerator(id, namaVisitor, "Dancing Animals", animalList.get(cat));
                                                System.out.print("Thank you for your interest. Would you like to register to other attractions?(Y/N): ");
                                                String ans2 = in.next();
                                                if (ans2.equalsIgnoreCase("y")) {
                                                    
                                                    continue;
                                                }
                                                else if(ans2.equalsIgnoreCase("n")){
                                                    RegisGenerator.regisNow(id);
                                                    
                                                    break; 
                                                    }
                                            } else continue;
                                        }
                                    }else if(thirdCommand==2){
                                        if (listAttraction[cat][pc]){
                                            System.out.print("One more step, please let us know your name: ");
                                            in.nextLine();
                                            String namaVisitor = in.nextLine();
                                            RegisGenerator.attractionGenerator("Passionate Coders", animalList.get(cat), namaVisitor);
                                            String ans = in.next();
                                            if (ans.equalsIgnoreCase("y")) {
                                                RegisGenerator.regisGenerator(id, namaVisitor, "Passionate Coders", animalList.get(cat));
                                                System.out.print("Thank you for your interest. Would you like to register to other attractions?(Y/N): ");
                                                String ans2 = in.next();
                                                if (ans2.equalsIgnoreCase("y")) {
                                                    
                                                    continue;
                                                }
                                                else if(ans2.equalsIgnoreCase("n")){
                                                    RegisGenerator.regisNow(id);
                                                    
                                                    break; 
                                                    }
                                            } else continue;
                                        }
                                    }
                                    
                                }
                            }
                        } else if(secCommand == 2) {
                            if(canDoAttraction[hamster] && cekAttraction(animalList.get(hamster))) {
                                System.out.print(animalAttraction("Hamster"));
                                String thirdAsk = in.next();
                                if(isNumeric(thirdAsk)) {
                                    int thirdCommand = Integer.parseInt(thirdAsk);
                                    if(thirdCommand==1){
                                        if (listAttraction[hamster][da]){
                                            System.out.print("One more step, please let us know your name: ");
                                            in.nextLine();
                                            String namaVisitor = in.nextLine();
                                            RegisGenerator.attractionGenerator("Dancing Animals", animalList.get(hamster), namaVisitor);
                                            String ans = in.next();
                                            if (ans.equalsIgnoreCase("y")) {
                                                RegisGenerator.regisGenerator(id, namaVisitor, "Dancing Animals", animalList.get(hamster));
                                                System.out.print("Thank you for your interest. Would you like to register to other attractions?(Y/N): ");
                                                String ans2 = in.next();
                                                if (ans2.equalsIgnoreCase("y")) {
                                                    
                                                    continue;
                                                }
                                                else if(ans2.equalsIgnoreCase("n")){
                                                    RegisGenerator.regisNow(id);
                                                    
                                                    break; 
                                                    }
                                            } else continue;
                                        }
                                    } else if(thirdCommand==2){
                                        if (listAttraction[hamster][cm]){
                                            System.out.print("One more step, please let us know your name: ");
                                            in.nextLine();
                                            String namaVisitor = in.nextLine();
                                            RegisGenerator.attractionGenerator("Counting Master", animalList.get(hamster), namaVisitor);
                                            String ans = in.next();
                                            if (ans.equalsIgnoreCase("y")) {
                                                RegisGenerator.regisGenerator(id, namaVisitor, "Counting Master", animalList.get(hamster));
                                                System.out.print("Thank you for your interest. Would you like to register to other attractions?(Y/N): ");
                                                String ans2 = in.next();
                                                if (ans2.equalsIgnoreCase("y")) {
                                                    
                                                    continue;
                                                }
                                                else if(ans2.equalsIgnoreCase("n")){
                                                    RegisGenerator.regisNow(id);
                                                    
                                                    break; 
                                                    }
                                            } else continue;
                                        }
                                    } else if(thirdCommand==3){
                                        if (listAttraction[hamster][pc]){
                                            System.out.print("One more step, please let us know your name: ");
                                            in.nextLine();
                                            String namaVisitor = in.nextLine();
                                            RegisGenerator.attractionGenerator("Passionate Coders", animalList.get(hamster), namaVisitor);
                                            String ans = in.next();
                                            if (ans.equalsIgnoreCase("y")) {
                                                RegisGenerator.regisGenerator(id, namaVisitor, "Passionate Coders", animalList.get(hamster));
                                                System.out.print("Thank you for your interest. Would you like to register to other attractions?(Y/N): ");
                                                String ans2 = in.next();
                                                if (ans2.equalsIgnoreCase("y")) {
                                                    
                                                    continue;
                                                }
                                                else if(ans2.equalsIgnoreCase("n")){
                                                    RegisGenerator.regisNow(id);
                                                    
                                                    break; 
                                                    }
                                            } else continue;
                                        }
                                    }
                                }
                            } 
                        } else if(secCommand == 3) {
                            if(canDoAttraction[lion] && cekAttraction(animalList.get(lion))) {
                                System.out.print(animalAttraction("Lion"));
                                String thirdAsk = in.next();
                                if(isNumeric(thirdAsk)) {
                                    int thirdCommand = Integer.parseInt(thirdAsk);
                                    if(thirdCommand==1){
                                        if (listAttraction[lion][cof]){
                                            System.out.print("One more step, please let us know your name: ");
                                            in.nextLine();
                                            String namaVisitor = in.nextLine();
                                            RegisGenerator.attractionGenerator("Circle of Fires", animalList.get(lion), namaVisitor);
                                            String ans = in.next();
                                            if (ans.equalsIgnoreCase("y")) {
                                                RegisGenerator.regisGenerator(id, namaVisitor, "Circle of Fires", animalList.get(lion));
                                                System.out.print("Thank you for your interest. Would you like to register to other attractions?(Y/N): ");
                                                String ans2 = in.next();
                                                if (ans2.equalsIgnoreCase("y")) {
                                                    
                                                    continue;
                                                }
                                                else if(ans2.equalsIgnoreCase("n")){
                                                    RegisGenerator.regisNow(id);
                                                    
                                                    break; 
                                                    }
                                            } else continue;
                                        }
                                    }
                                }
                            }
                        } else if(secCommand == 4) {
                            if(canDoAttraction[whale] && cekAttraction(animalList.get(whale))) {
                                System.out.print(animalAttraction("Whale"));
                                String thirdAsk = in.next();
                                if(isNumeric(thirdAsk)) {
                                    int thirdCommand = Integer.parseInt(thirdAsk);
                                    if(thirdCommand==1){
                                        if (listAttraction[whale][cof]){
                                            System.out.print("One more step, please let us know your name: ");
                                            in.nextLine();
                                            String namaVisitor = in.nextLine();
                                            RegisGenerator.attractionGenerator("Circle of Fires", animalList.get(whale), namaVisitor);
                                            String ans = in.next();
                                            if (ans.equalsIgnoreCase("y")) {
                                                RegisGenerator.regisGenerator(id, namaVisitor, "Circle of Fires", animalList.get(whale));
                                                System.out.print("Thank you for your interest. Would you like to register to other attractions?(Y/N): ");
                                                String ans2 = in.next();
                                                if (ans2.equalsIgnoreCase("y")) {
                                                    
                                                    continue;
                                                }
                                                else if(ans2.equalsIgnoreCase("n")){
                                                    RegisGenerator.regisNow(id);
                                                    
                                                    break; 
                                                    }
                                            } else continue;
                                        }
                                    }else if(thirdCommand==2){
                                        if (listAttraction[whale][cm]){
                                            System.out.print("One more step, please let us know your name: ");
                                            in.nextLine();
                                            String namaVisitor = in.nextLine();
                                            RegisGenerator.attractionGenerator("Counting Master", animalList.get(whale), namaVisitor);
                                            String ans = in.next();
                                            if (ans.equalsIgnoreCase("y")) {
                                                RegisGenerator.regisGenerator(id, namaVisitor, "Counting Master", animalList.get(whale));
                                                System.out.print("Thank you for your interest. Would you like to register to other attractions?(Y/N): ");
                                                String ans2 = in.next();
                                                if (ans2.equalsIgnoreCase("y")) {
                                                    
                                                    continue;
                                                }
                                                else if(ans2.equalsIgnoreCase("n")){
                                                    RegisGenerator.regisNow(id);
                                                    
                                                    break; 
                                                    }
                                            } else continue;
                                        }
                                    }
                                }
                            }
                        } else {
                            continue;
                        }
                    }
                } else if (command==2) {
                    if (!correctSection[1]) {
                        System.out.println("Unfortunately, there is no World of Aves recorded, please choose other sections");
                        continue;
                    }
                    System.out.println(textAnimalAves);
                    System.out.print("Please choose your preferred animals (type the number): ");
                    String secAsk = in.next();
                    if (isNumeric(secAsk)) {
                        int secCommand = Integer.parseInt(secAsk);
                        if (secCommand == 1) {
                            if(cekAttraction(animalList.get(eagle)) && canDoAttraction[eagle]) {
                                System.out.print(animalAttraction("Eagle"));
                                String thirdAsk = in.next();
                                if(isNumeric(thirdAsk)) {
                                    int thirdCommand = Integer.parseInt(thirdAsk);
                                    if(thirdCommand==1){
                                        if (listAttraction[eagle][cof]){
                                            System.out.print("One more step, please let us know your name: ");
                                            in.nextLine();
                                            String namaVisitor = in.nextLine();
                                            RegisGenerator.attractionGenerator("Circle of Fires", animalList.get(eagle), namaVisitor);
                                            String ans = in.next();
                                            if (ans.equalsIgnoreCase("y")) {
                                                RegisGenerator.regisGenerator(id, namaVisitor, "Circle of Fires", animalList.get(eagle));
                                                System.out.print("Thank you for your interest. Would you like to register to other attractions?(Y/N): ");
                                                String ans2 = in.next();
                                                if (ans2.equalsIgnoreCase("y")) {
                                                    
                                                    continue;
                                                }
                                                else if(ans2.equalsIgnoreCase("n")){
                                                    RegisGenerator.regisNow(id);
                                                    
                                                    break; 
                                                    }
                                            } else continue;
                                        }
                                    }
                                }
                            }
                        } else if(secCommand == 2) {
                            if(canDoAttraction[parrot] && cekAttraction(animalList.get(parrot))) {
                                System.out.print(animalAttraction("Parrot"));
                                String thirdAsk = in.next();
                                if(isNumeric(thirdAsk)) {
                                    int thirdCommand = Integer.parseInt(thirdAsk);
                                    if(thirdCommand==1){
                                        if (listAttraction[parrot][da]){
                                            System.out.print("One more step, please let us know your name: ");
                                            in.nextLine();
                                            String namaVisitor = in.nextLine();
                                            RegisGenerator.attractionGenerator("Dancing Animals", animalList.get(parrot), namaVisitor);
                                            String ans = in.next();
                                            if (ans.equalsIgnoreCase("y")) {
                                                RegisGenerator.regisGenerator(id, namaVisitor, "Dancing Animals", animalList.get(parrot));
                                                System.out.print("Thank you for your interest. Would you like to register to other attractions?(Y/N): ");
                                                String ans2 = in.next();
                                                if (ans2.equalsIgnoreCase("y")) {
                                                    
                                                    continue;
                                                }
                                                else if(ans2.equalsIgnoreCase("n")){
                                                    RegisGenerator.regisNow(id);
                                                    
                                                    break; 
                                                    }
                                            } else continue;
                                        }
                                    } else if(thirdCommand==2){
                                        if (listAttraction[parrot][cm]){
                                            System.out.print("One more step, please let us know your name: ");
                                            in.nextLine();
                                            String namaVisitor = in.nextLine();
                                            RegisGenerator.attractionGenerator("Counting Master", animalList.get(parrot), namaVisitor);
                                            String ans = in.next();
                                            if (ans.equalsIgnoreCase("y")) {
                                                RegisGenerator.regisGenerator(id, namaVisitor, "Counting Master", animalList.get(parrot));
                                                System.out.print("Thank you for your interest. Would you like to register to other attractions?(Y/N): ");
                                                String ans2 = in.next();
                                                if (ans2.equalsIgnoreCase("y")) {
                                                    
                                                    continue;
                                                }
                                                else if(ans2.equalsIgnoreCase("n")){
                                                    RegisGenerator.regisNow(id);
                                                    
                                                    break; 
                                                    }
                                            } else continue;
                                        }
                                    }
                                }
                            }
                        }  else {
                            continue;
                        }
                    }
                } else if (command==3) {
                    if (!correctSection[2]) {
                        System.out.println("Unfortunately, there is no Reptilian Kingdom recorded, please choose other sections");
                        continue;
                    }
                    System.out.println(textAnimalReptil);
                    System.out.print("Please choose your preferred animals (type the number): ");
                    String secAsk = in.next();
                    if (isNumeric(secAsk)) {
                        int secCommand = Integer.parseInt(secAsk);
                        if (secCommand == 1) {
                            if(cekAttraction(animalList.get(snake)) && canDoAttraction[snake]) {
                                System.out.print(animalAttraction("Snake"));
                                String thirdAsk = in.next();
                                if(isNumeric(thirdAsk)) {
                                    int thirdCommand = Integer.parseInt(thirdAsk);
                                    if(thirdCommand==1){
                                        if (listAttraction[snake][da]){
                                            System.out.print("One more step, please let us know your name: ");
                                            in.nextLine();
                                            String namaVisitor = in.nextLine();
                                            RegisGenerator.attractionGenerator("Dancing Animals", animalList.get(snake), namaVisitor);
                                            String ans = in.next();
                                            if (ans.equalsIgnoreCase("y")) {
                                                RegisGenerator.regisGenerator(id, namaVisitor, "Dancing Animals", animalList.get(snake));
                                                System.out.print("Thank you for your interest. Would you like to register to other attractions?(Y/N): ");
                                                String ans2 = in.next();
                                                if (ans2.equalsIgnoreCase("y")) {
                                                    
                                                    continue;
                                                }
                                                else if(ans2.equalsIgnoreCase("n")){
                                                    RegisGenerator.regisNow(id);
                                                    
                                                    break; 
                                                    }
                                            } else continue;
                                        } 
                                    }else if(thirdCommand==2){
                                        if (listAttraction[snake][pc]){
                                            System.out.print("One more step, please let us know your name: ");
                                            in.nextLine();
                                            String namaVisitor = in.nextLine();
                                            RegisGenerator.attractionGenerator("Passionate Coders", animalList.get(snake), namaVisitor);
                                            String ans = in.next();
                                            if (ans.equalsIgnoreCase("y")) {
                                                RegisGenerator.regisGenerator(id, namaVisitor, "Passionate Coders", animalList.get(snake));
                                                System.out.print("Thank you for your interest. Would you like to register to other attractions?(Y/N): ");
                                                String ans2 = in.next();
                                                if (ans2.equalsIgnoreCase("y")) {
                                                    
                                                    continue;
                                                }
                                                else if(ans2.equalsIgnoreCase("n")){
                                                    RegisGenerator.regisNow(id);
                                                    
                                                    break; 
                                                    }
                                            } else continue;
                                        }
                                    }
                                }
                            }
                        } else {
                            continue;
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Wrong format");
            }
        }
    }
    public static boolean isNumeric(String x) { //Cek apakah input merupakan angka apa bukan
        try {
            int i = Integer.parseInt(x);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
    public static boolean cekAttraction(ArrayList<Animal> listAnimal) { //Cek apakah ada hewan yang bisa melakukan attraksi apa tidak
        for (Animal animal: listAnimal) {
            if(animal.circleOfFire() || animal.passionateCoders() || animal.dancingAnimal() || animal.countingMaster()) return true;
        }
        return false;
    }
    public static String animalAttraction(String animal) { //String generator untuk setiap hewan
        String text = "---"+animal+"---\nAttraction by "+animal+"\n";
        int count = 1;
        if (animal.equals("Lion")) {
            text += "1. Circles of Fires";
        } else if (animal.equals("Hamster")) {
            text += "1. Dancing Animals\n2. Counting Masters\n3. Passionate Coders";
        } else if (animal.equals("Whale")) {
            text += "1. Circles of Fires\n2. Counting Masters";
        } else if (animal.equals("Eagle")) {
            text += "1. Circle of Fires";
        } else if(animal.equals("Snake")) {
            text += "1. Dancing Animals\n2. Passionate Coders";
        } else if (animal.equals("Cat")) {
            text += "1. Dancing Animals\n2. Passionate Coders";
        } else if (animal.equals("Parrot")) {
            text += "1. Dancing Animals\n2. Counting Masters";
        }
        text += "\nPlease choose your preferred attractions (type the number): ";
        return text;
    }
}

