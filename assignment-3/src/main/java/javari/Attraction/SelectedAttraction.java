package Attraction;
import java.util.List;
import java.util.ArrayList;
import Animal.Animal;
public class SelectedAttraction { //Objek Attraksi yang sudah dimasukkan
    private String nama;
    private String type;
    private List<Animal> performer;
    public SelectedAttraction(String nama, String type) {
        this.nama = nama;
        this.type = type;
        this.performer = new ArrayList<Animal>();
    }
    public String getNama(){
        return this.nama;
    }
    public String getType(){
        return this.type;
    }
    public List<Animal> getPerformers() {
        return this.performer;
    }
    public boolean addPerformer(Animal performer) {
        if (performer instanceof Animal) {
            this.performer.add(performer);
            return true;
        }
        return false;
    }
}