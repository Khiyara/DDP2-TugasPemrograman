public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms
	// TODO Complete me!
    WildCat cat;
	TrainCar next;
	static int count;
    public TrainCar(WildCat cat) {
        // TODO Complete me!
		this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        // TODO Complete me!
		this.next = next;
		this.cat = cat;
    }

    public double computeTotalWeight() {
        // TODO Complete me!
		if (this.next == null) {
			return this.cat.weight + EMPTY_WEIGHT;
		}
		else{
			return this.cat.weight + EMPTY_WEIGHT + this.next.computeTotalWeight();
		}
    }

    public double computeTotalMassIndex() {
        // TODO Complete me!
		count++;
		if (this.next == null) {
			return this.cat.computeMassIndex();
		}
        return this.cat.computeMassIndex() + this.next.computeTotalMassIndex();
    }

    public void printCar() {
        // TODO Complete me!
		if (this.next == null) {
			System.out.print("-("+this.cat.name+")\n");
			return;
		}
		System.out.print("-("+this.cat.name+")-");
		this.next.printCar();
    }
	
	public void setNext(TrainCar next) {
		this.next = next;
	}
}
