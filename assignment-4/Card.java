import javax.swing.JButton;
/**
    Referensi:
    https://codereview.stackexchange.com/questions/85833/basic-memory-match-game-in-java
    Setiap kartu memiliki ID yang berbeda
    Oleh karena itu setiap kartu akan berbeda gambarnya
*/
public class Card extends JButton{
    private String id;
    public static String[] listImg = {"image/1.png","image/2.png","image/3.png","image/4.png","image/5.png","image/6.png","image/7.png","image/8.png","image/9.png","image/10.png","image/11.png","image/12.png","image/13.png","image/14.png","image/15.png","image/16.png","image/17.png","image/18.png"};
    private boolean matched = false;

    public void setId(int id){
        this.id = listImg[id];
    }
    public String getId(){
        return this.id;
    }
    public void setMatched(boolean matched){
        this.matched = matched;
    }

    public boolean getMatched(){
        return this.matched;
    }
}
