import java.awt.Dimension;
import javax.swing.JFrame;
/**
    Referensi:
    https://codereview.stackexchange.com/questions/85833/basic-memory-match-game-in-java
    Buat frame / window
*/
public class Game{
    public static void main(String[] args){
        Board b = new Board();
        b.setPreferredSize(new Dimension(700,700)); 
        b.setLocation(400, 50);
        b.setResizable(true);
        //b.setMinimumSize(new Dimension(600,600));
        //b.setMaximumSize(new Dimension(600,600));
        b.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        b.pack();
        b.setVisible(true);
    }   
}

