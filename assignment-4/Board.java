import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.imageio.ImageIO;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.io.IOException;
import java.io.File;
/** 
    Referensi:
    https://codereview.stackexchange.com/questions/85833/basic-memory-match-game-in-java
*/
public class Board extends JFrame{
    /**
        Attribute board, memiliki Frame sendiri,
        ada punya label,button dan fungsi2 dari setiap button
    */
    private List<Card> cards;
    private Card selectedCard;
    private Card c1;
    private Card c2;
    private Timer t;
    private JLabel counter;
    private Container pane;
    private int count = 0;
    private List<Card> cardsList = new ArrayList<Card>();
    private List<Integer> cardVals = new ArrayList<Integer>();
    public Board(){
        /**
            Setup fungsi board yang diperlukan
        */
        int pairs = 18;
        for (int i = 0; i < pairs; i++){
            cardVals.add(i);
            cardVals.add(i);
        }
        Collections.shuffle(cardVals);
        for (int val : cardVals){
            Card c = new Card();
            c.setId(val);
            c.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae){
                    selectedCard = c;
                    doTurn();
                }
            });
            cardsList.add(c);
        }
        this.cards = cardsList;

        //set up the board itself
        pane = getContentPane();
        pane.setLayout(new GridLayout(7, 6));
        try {
            for (Card c : cards){
                pane.add(c);
                Image image = ImageIO.read(new File("image/cardback.png"));
                c.setIcon(new ImageIcon(image.getScaledInstance(90,70,java.awt.Image.SCALE_SMOOTH)));
            }
        } catch (IOException e) {
            JOptionPane.showMessageDialog(this, "File not found error!");
            System.exit(0);
        }
        
        JLabel txt = new JLabel("Counter tries: ",10);
        add(txt);
        
        counter = new JLabel("    "+count+"");
        add(counter);
       
        t = new Timer(1000, new ActionListener(){
            public void actionPerformed(ActionEvent ae){
                ++count;
                counter.setText("    "+count+"");
                checkCards();
            }
        });
        t.setRepeats(false);
        JButton btn = new JButton("Restart");
        btn.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent x){
                restart();
                count = 0;
                counter.setText("    "+count+"");
            }
        });  
        add(btn);     
        JButton btnExit = new JButton("Exit");
        btnExit.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent x){
                exit();
            }
        });
        add(btnExit);
        setTitle("Hearthstone Card Match Game");
    }
    /**
        Fungsi tombol restart, mengacak semua kartu, dan menset jadi setMatch(false)
    */
    public void restart() {
        Collections.shuffle(this.cardVals);
        int i = 0;
        for (Card c : this.cards){
            c.setId(this.cardVals.get(i));
            i++;
        }
        try{
            for(Card c: this.cards){
                if (c.getMatched()){
                    c.setMatched(false);
                    c.setEnabled(true);
                    c.setVisible(true);
                    Image image = ImageIO.read(new File("image/cardback.png"));
                    c.setIcon(new ImageIcon(image.getScaledInstance(90,70,java.awt.Image.SCALE_SMOOTH)));
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "File not found error!");
            System.exit(0);
        }
        
        
    }
    /**
        Membuka kartu, menampilkan gambar
        setiap pemilihan kartu dua, clock akan jalan
    */
    public void doTurn() {
        try {
            if (c1 == null && c2 == null){
                c1 = selectedCard;
                Image image = ImageIO.read(new File(String.valueOf(c1.getId())));
                c1.setIcon(new ImageIcon(image.getScaledInstance(90,70,java.awt.Image.SCALE_SMOOTH)));
            }

            if (c1 != null && c1 != selectedCard && c2 == null){
                c2 = selectedCard;
                Image image = ImageIO.read(new File(String.valueOf(c2.getId())));
                c2.setIcon(new ImageIcon(image.getScaledInstance(90,70,java.awt.Image.SCALE_SMOOTH)));
                t.start();
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "File not found error!");
            System.exit(0);
        }
        
    }
    /**
        Cek kartu pertama dan kedua jika sama maka button di disables
        jika berbeda kembalikan ke cardback
    */
    public void checkCards() {
        if (c1.getId() == c2.getId()){//match condition
            c1.setEnabled(false); //disables the button
            c2.setEnabled(false);
            c1.setMatched(true); //flags the button as having been matched
            c2.setMatched(true);
            c1.setVisible(false);
            c2.setVisible(false);
            if (this.isGameWon()){
                int result = JOptionPane.showConfirmDialog(this, "Play Again?" ,"You Win!" , JOptionPane.YES_NO_OPTION);
                if (result == JOptionPane.NO_OPTION) System.exit(0);
                else if(result == JOptionPane.YES_OPTION) {
                    restart();
                    count = 0;
                    counter.setText("    "+count+"");
                }
            }
        }

        else {
            try{
                Image image = ImageIO.read(new File("image/cardback.png"));
                c1.setIcon(new ImageIcon(image.getScaledInstance(90,70,java.awt.Image.SCALE_SMOOTH)));
                Image image2 = ImageIO.read(new File("image/cardback.png"));
                c2.setIcon(new ImageIcon(image2.getScaledInstance(90,70,java.awt.Image.SCALE_SMOOTH)));
            } catch (IOException e) {
                JOptionPane.showMessageDialog(this, "File not found error!");
                System.exit(0);
            }
            
            
        }
        c1 = null; //reset c1 and c2
        c2 = null;
    }
    /**
        Cek apakah game sudah selesai (jika semua sudah terChecked)
    */
    public boolean isGameWon() {
        for(Card c: this.cards){
            if (c.getMatched() == false){
                return false;
            }
        }
        return true;
    }
    /**
        Fungsi tombol exit
    */
    public void exit() {
        int result = JOptionPane.showConfirmDialog(this, "Are you sure you want to quit?" ,"Exit program" , JOptionPane.YES_NO_OPTION);
        if (result == JOptionPane.YES_OPTION) System.exit(0);
    }

}