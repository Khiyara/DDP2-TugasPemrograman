package Animal;
import java.util.Random;
import Animal.Animal;
public class Cat extends Animal {
    static Random random = new Random();
    public Cat(String nama, int length, String kategori) {
        super(nama, length, kategori);
    }    
    public void cuddled() {
        int x = random.nextInt(3);
        String voice ="";
        if (x==0) voice = "Miaaaw..";
        else if (x==1) voice = "Purrr..";
        else if (x==2) voice = "Mwaw!";
        else voice = "Mraaawr!";
        System.out.println(getNama()+" makes a voice: "+voice);
        System.out.println("Back to the office!");
    }
    public void brushed() {
        System.out.println("Time to clean "+getNama()+"\'s fur");
        System.out.println(getNama()+" makes a voice: Nyaaan..");
        System.out.println("Back to the office!");
    }
}