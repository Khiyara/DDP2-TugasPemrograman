package Animal;
import Animal.Animal;
public class Hamster extends Animal {
    public Hamster(String nama, int length, String kategori){
        super(nama, length, kategori);
    }
    public void gnawing() {
        System.out.println(getNama()+" makes a voice: ngkrrit...ngkriiitt");
        System.out.println("Back to the office!");
    }
    public void run() {
        System.out.println(getNama()+" makes a voice: trrr...trrr");
        System.out.println("Back to the office!");
    }
}