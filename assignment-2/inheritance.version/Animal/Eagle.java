package Animal;
import Animal.Animal;
public class Eagle extends Animal {
    public Eagle(String nama, int length, String kategori){
        super(nama, length, kategori);
    }
    public void fly() {
        System.out.println(getNama()+" makes a voice: kwaakk...");
        System.out.println("You hurt!");
        System.out.println("Back to the office!");
    }
}