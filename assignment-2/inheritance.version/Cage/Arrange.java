package Cage;
import java.util.ArrayList;
import java.util.Scanner;
import Animal.*;
public class Arrange {
    static Scanner input = new Scanner(System.in);
    static ArrayList<ArrayList<ArrayList<Cage>>> cagesIn;
    static ArrayList<ArrayList<ArrayList<Cage>>> cagesInCopy;
    static ArrayList<ArrayList<ArrayList<Cage>>> cagesOut;
    static ArrayList<ArrayList<ArrayList<Cage>>> cagesOutCopy;
    static Animal[][] animalCats;
    static Animal[][] animalParrot;
    static Animal[][] animalHamster;
    static Animal[][] animalEagles;
    static Animal[][] animalLion;
    
    public static void setCageIn(Animal[][] animals, int x, String nama, int tempat, ArrayList<ArrayList<ArrayList<Cage>>> cage, ArrayList<ArrayList<ArrayList<Cage>>> cageCopy, String kategori) {
        int c = 0;
        int z = 0;
        int temp;
        if (x%3==2) temp = 1;
        else temp = 0;
        if (x > 0 && nama.equals("cat")){
            for (int i=0;i<3;i++) {
                cage.get(tempat).add(new ArrayList<Cage>());
                cageCopy.get(tempat).add(new ArrayList<Cage>());
            }
            System.out.println("Provide the information of "+nama+"(s):");
            String Prov = input.next();
            String[] info = Prov.split(",");
            if (x==1) {
                for (int i=0;i<1;i++) {
                    String[] pisah = info[i].split("\\|");
                    animals[c+2][z] = new Cat(pisah[0],Integer.parseInt(pisah[1]),kategori);
                    cage.get(tempat).get(c+2).add(new Cage(animals[c+2][z])); 
                    cageCopy.get(tempat).get(c+2).add(new Cage(animals[c+2][z])); return;
                }
            } else if (x==2) {
                for (int i=1;i<2;i++) {
                    String[] pisah = info[i].split("\\|");
                    animals[c+1][z] = new Cat(pisah[0],Integer.parseInt(pisah[1]),kategori);
                    cage.get(tempat).get(c+1).add(new Cage(animals[c+1][z]));
                    cageCopy.get(tempat).get(c+1).add(new Cage(animals[c+1][z]));
                }
                for (int i=0;i<1;i++) {
                    String[] pisah = info[i].split("\\|");
                    animals[c+2][z] = new Cat(pisah[0],Integer.parseInt(pisah[1]),kategori);
                    cage.get(tempat).get(c+2).add(new Cage(animals[c+2][z])); 
                    cageCopy.get(tempat).get(c+2).add(new Cage(animals[c+2][z])); return;
                }
            }
            for (int i=((x/3)*2+temp);i<x;i++) {
                String[] pisah = info[i].split("\\|");
                animals[c][i] = new Cat(pisah[0],Integer.parseInt(pisah[1]),kategori);
                cage.get(tempat).get(c).add(new Cage(animals[c][i]));
                cageCopy.get(tempat).get(c).add(new Cage(animals[c][i]));
            }
            for (int i=x/3;i<((x/3)*2+temp);i++) {
                String[] pisah = info[i].split("\\|");
                animals[c+1][z] = new Cat(pisah[0],Integer.parseInt(pisah[1]),kategori);
                cage.get(tempat).get(c+1).add(new Cage(animals[c+1][z]));
                cageCopy.get(tempat).get(c+1).add(new Cage(animals[c+1][z]));
                z++;
            }
            z = 0;
            for (int i=0;i<x/3;i++) {
                String[] pisah = info[i].split("\\|");
                animals[c+2][z] = new Cat(pisah[0],Integer.parseInt(pisah[1]),kategori);
                cage.get(tempat).get(c+2).add(new Cage(animals[c+2][z]));
                cageCopy.get(tempat).get(c+2).add(new Cage(animals[c+2][z]));
                z++;
            }
            
        } else if (x > 0 && nama.equals("parrot")){
            for (int i=0;i<3;i++) {
                cage.get(tempat).add(new ArrayList<Cage>());
                cageCopy.get(tempat).add(new ArrayList<Cage>());
            }
            System.out.println("Provide the information of "+nama+"(s):");
            String Prov = input.next();
            String[] info = Prov.split(",");
            if (x==1) {
                for (int i=0;i<1;i++) {
                    String[] pisah = info[i].split("\\|");
                    animals[c+2][z] = new Parrot(pisah[0],Integer.parseInt(pisah[1]),kategori);
                    cage.get(tempat).get(c+2).add(new Cage(animals[c+2][z])); 
                    cageCopy.get(tempat).get(c+2).add(new Cage(animals[c+2][z])); return;
                }
            } else if (x==2) {
                for (int i=1;i<2;i++) {
                    String[] pisah = info[i].split("\\|");
                    animals[c+1][z] = new Parrot(pisah[0],Integer.parseInt(pisah[1]),kategori);
                    cage.get(tempat).get(c+1).add(new Cage(animals[c+1][z]));
                    cageCopy.get(tempat).get(c+1).add(new Cage(animals[c+1][z]));
                }
                for (int i=0;i<1;i++) {
                    String[] pisah = info[i].split("\\|");
                    animals[c+2][z] = new Parrot(pisah[0],Integer.parseInt(pisah[1]),kategori);
                    cage.get(tempat).get(c+2).add(new Cage(animals[c+2][z])); 
                    cageCopy.get(tempat).get(c+2).add(new Cage(animals[c+2][z])); return;
                }
            }
            
            for (int i=((x/3)*2+temp);i<x;i++) {
                String[] pisah = info[i].split("\\|");
                animals[c][i] = new Parrot(pisah[0],Integer.parseInt(pisah[1]),kategori);
                cage.get(tempat).get(c).add(new Cage(animals[c][i]));
                cageCopy.get(tempat).get(c).add(new Cage(animals[c][i]));
            }
            for (int i=x/3;i<((x/3)*2)+temp;i++) {
                String[] pisah = info[i].split("\\|");
                animals[c+1][z] = new Parrot(pisah[0],Integer.parseInt(pisah[1]),kategori);
                cage.get(tempat).get(c+1).add(new Cage(animals[c+1][z]));
                cageCopy.get(tempat).get(c+1).add(new Cage(animals[c+1][z]));
                z++;
            }
            z = 0;
            for (int i=0;i<x/3;i++) {
                String[] pisah = info[i].split("\\|");
                animals[c+2][z] = new Parrot(pisah[0],Integer.parseInt(pisah[1]),kategori);
                cage.get(tempat).get(c+2).add(new Cage(animals[c+2][z]));
                cageCopy.get(tempat).get(c+2).add(new Cage(animals[c+2][z]));
                z++;
            }
        } else if (x > 0 && nama.equals("eagle")){
            for (int i=0;i<3;i++) {
                cage.get(tempat).add(new ArrayList<Cage>());
                cageCopy.get(tempat).add(new ArrayList<Cage>());
            }
            System.out.println("Provide the information of "+nama+"(s):");
            String Prov = input.next();
            String[] info = Prov.split(",");
            if (x==1) {
                for (int i=0;i<1;i++) {
                    String[] pisah = info[i].split("\\|");
                    animals[c+2][z] = new Eagle(pisah[0],Integer.parseInt(pisah[1]),kategori);
                    cage.get(tempat).get(c+2).add(new Cage(animals[c+2][z])); 
                    cageCopy.get(tempat).get(c+2).add(new Cage(animals[c+2][z])); return;
                }
            } else if (x==2) {
                for (int i=1;i<2;i++) {
                    String[] pisah = info[i].split("\\|");
                    animals[c+1][z] = new Eagle(pisah[0],Integer.parseInt(pisah[1]),kategori);
                    cage.get(tempat).get(c+1).add(new Cage(animals[c+1][z]));
                    cageCopy.get(tempat).get(c+1).add(new Cage(animals[c+1][z]));
                }
                for (int i=0;i<1;i++) {
                    String[] pisah = info[i].split("\\|");
                    animals[c+2][z] = new Eagle(pisah[0],Integer.parseInt(pisah[1]),kategori);
                    cage.get(tempat).get(c+2).add(new Cage(animals[c+2][z])); 
                    cageCopy.get(tempat).get(c+2).add(new Cage(animals[c+2][z])); return;
                }
            }        
            for (int i=((x/3)*2)+temp;i<x;i++) {
                String[] pisah = info[i].split("\\|");
                animals[c][i] = new Eagle(pisah[0],Integer.parseInt(pisah[1]),kategori);
                cage.get(tempat).get(c).add(new Cage(animals[c][i]));
                cageCopy.get(tempat).get(c).add(new Cage(animals[c][i]));
            }
            for (int i=x/3;i<((x/3)*2)+temp;i++) {
                String[] pisah = info[i].split("\\|");
                animals[c+1][z] = new Eagle(pisah[0],Integer.parseInt(pisah[1]),kategori);
                cage.get(tempat).get(c+1).add(new Cage(animals[c+1][z]));
                cageCopy.get(tempat).get(c+1).add(new Cage(animals[c+1][z]));
                z++;
            }
            z = 0;
            for (int i=0;i<x/3;i++) {
                String[] pisah = info[i].split("\\|");
                animals[c+2][z] = new Eagle(pisah[0],Integer.parseInt(pisah[1]),kategori);
                cage.get(tempat).get(c+2).add(new Cage(animals[c+2][z]));
                cageCopy.get(tempat).get(c+2).add(new Cage(animals[c+2][z]));
                z++;
            }
        } else if (x > 0 && nama.equals("hamster")){
            for (int i=0;i<3;i++) {
                cage.get(tempat).add(new ArrayList<Cage>());
                cageCopy.get(tempat).add(new ArrayList<Cage>());
            }
            System.out.println("Provide the information of "+nama+"(s):");
            String Prov = input.next();
            String[] info = Prov.split(",");
            if (x==1) {
                for (int i=0;i<1;i++) {
                    String[] pisah = info[i].split("\\|");
                    animals[c+2][z] = new Hamster(pisah[0],Integer.parseInt(pisah[1]),kategori);
                    cage.get(tempat).get(c+2).add(new Cage(animals[c+2][z])); 
                    cageCopy.get(tempat).get(c+2).add(new Cage(animals[c+2][z])); return;
                }
            } else if (x==2) {
                for (int i=1;i<2;i++) {
                    String[] pisah = info[i].split("\\|");
                    animals[c+1][z] = new Hamster(pisah[0],Integer.parseInt(pisah[1]),kategori);
                    cage.get(tempat).get(c+1).add(new Cage(animals[c+1][z]));
                    cageCopy.get(tempat).get(c+1).add(new Cage(animals[c+1][z]));
                }
                for (int i=0;i<1;i++) {
                    String[] pisah = info[i].split("\\|");
                    animals[c+2][z] = new Hamster(pisah[0],Integer.parseInt(pisah[1]),kategori);
                    cage.get(tempat).get(c+2).add(new Cage(animals[c+2][z])); 
                    cageCopy.get(tempat).get(c+2).add(new Cage(animals[c+2][z])); return;
                }
            }
            for (int i=((x/3)*2)+temp;i<x;i++) {
                String[] pisah = info[i].split("\\|");
                animals[c][i] = new Hamster(pisah[0],Integer.parseInt(pisah[1]),kategori);
                cage.get(tempat).get(c).add(new Cage(animals[c][i]));
                cageCopy.get(tempat).get(c).add(new Cage(animals[c][i]));
            }
            for (int i=x/3;i<((x/3)*2)+temp;i++) {
                String[] pisah = info[i].split("\\|");
                animals[c+1][z] = new Hamster(pisah[0],Integer.parseInt(pisah[1]),kategori);
                cage.get(tempat).get(c+1).add(new Cage(animals[c+1][z]));
                cageCopy.get(tempat).get(c+1).add(new Cage(animals[c+1][z]));
                z++;
            }
            z = 0;
            for (int i=0;i<x/3;i++) {
                String[] pisah = info[i].split("\\|");
                animals[c+2][z] = new Hamster(pisah[0],Integer.parseInt(pisah[1]),kategori);
                cage.get(tempat).get(c+2).add(new Cage(animals[c+2][z]));
                cageCopy.get(tempat).get(c+2).add(new Cage(animals[c+2][z]));
                z++;
            }
        } else if (x > 0 && nama.equals("lion")){
            for (int i=0;i<3;i++) {
                cage.get(tempat).add(new ArrayList<Cage>());
                cageCopy.get(tempat).add(new ArrayList<Cage>());
            }
            System.out.println("Provide the information of "+nama+"(s):");
            String Prov = input.next();
            String[] info = Prov.split(",");
            if (x==1) {
                for (int i=0;i<1;i++) {
                    String[] pisah = info[i].split("\\|");
                    animals[c+2][z] = new Lion(pisah[0],Integer.parseInt(pisah[1]),kategori);
                    cage.get(tempat).get(c+2).add(new Cage(animals[c+2][z])); 
                    cageCopy.get(tempat).get(c+2).add(new Cage(animals[c+2][z])); return;
                }
            } else if (x==2) {
                for (int i=1;i<2;i++) {
                    String[] pisah = info[i].split("\\|");
                    animals[c+1][z] = new Lion(pisah[0],Integer.parseInt(pisah[1]),kategori);
                    cage.get(tempat).get(c+1).add(new Cage(animals[c+1][z]));
                    cageCopy.get(tempat).get(c+1).add(new Cage(animals[c+1][z]));
                }
                for (int i=0;i<1;i++) {
                    String[] pisah = info[i].split("\\|");
                    animals[c+2][z] = new Lion(pisah[0],Integer.parseInt(pisah[1]),kategori);
                    cage.get(tempat).get(c+2).add(new Cage(animals[c+2][z])); 
                    cageCopy.get(tempat).get(c+2).add(new Cage(animals[c+2][z])); return;
                }
            }
            
            for (int i=((x/3)*2)+temp;i<x;i++) {
                String[] pisah = info[i].split("\\|");
                animals[c][i] = new Lion(pisah[0],Integer.parseInt(pisah[1]),kategori);
                cage.get(tempat).get(c).add(new Cage(animals[c][i]));
                cageCopy.get(tempat).get(c).add(new Cage(animals[c][i]));
            }
            for (int i=x/3;i<((x/3)*2)+temp;i++) {
                String[] pisah = info[i].split("\\|");
                animals[c+1][z] = new Lion(pisah[0],Integer.parseInt(pisah[1]),kategori);
                cage.get(tempat).get(c+1).add(new Cage(animals[c+1][z]));
                cageCopy.get(tempat).get(c+1).add(new Cage(animals[c+1][z]));
                z++;
            }
            z = 0;
            for (int i=0;i<x/3;i++) {
                String[] pisah = info[i].split("\\|");
                animals[c+2][z] = new Lion(pisah[0],Integer.parseInt(pisah[1]),kategori);
                cage.get(tempat).get(c+2).add(new Cage(animals[c+2][z]));
                cageCopy.get(tempat).get(c+2).add(new Cage(animals[c+2][z]));
                z++;
            }
        }
    }
    public static void ArrangeCage(ArrayList<ArrayList<ArrayList<Cage>>> cage, ArrayList<ArrayList<ArrayList<Cage>>> cageCopy, int tempat, String namaTempat) {
        int sizeCopy = cageCopy.get(tempat).size();
        System.out.println("Location "+namaTempat);
        int x = 3;
        for(int i=0;i<sizeCopy;i++){
            System.out.print("Level "+x+": ");
            for(int j=0;j<cageCopy.get(tempat).get(i).size();j++){
                System.out.print(cage.get(tempat).get(i).get(j).getAnimal().getNama()+"("+cage.get(tempat).get(i).get(j).getAnimal().getLength()+"-"+cage.get(tempat).get(i).get(j).getType()+"),");   
            }
            System.out.println();
            x--;
        }
        for(int i=0;i<cageCopy.get(tempat).size();i++){
            cage.get(tempat).remove(0);
        }for (int i=0;i<3;i++) {
            cage.get(tempat).add(new ArrayList<Cage>());
        }
        int c = 0;
        int z = 1;
        for(int i=0;i<cageCopy.get(tempat).size();i++) {
            for (int j=cageCopy.get(tempat).get(z).size();j>0;j--) {           
                cage.get(tempat).get(c).add(cageCopy.get(tempat).get(z).get(j-1));
            }
            c++; z++;
            if (z==3) z = 0;
        }   
        x = 3;
        System.out.println("\nAfter Arrangement...");
        for(int i=0;i<cage.get(tempat).size();i++) {
            System.out.print("Level "+x+": ");
            for (int j=0;j<cage.get(tempat).get(i).size();j++) {
                System.out.print(cage.get(tempat).get(i).get(j).getAnimal().getNama()+"("+cage.get(tempat).get(i).get(j).getAnimal().getLength()+"-"+cage.get(tempat).get(i).get(j).getType()+"),");
            }
            System.out.println();
            x--;
        }System.out.println();
    }
}