import java.util.Random;
public class Animal {
    static Random random = new Random();
    boolean isLion = false;
    boolean isCat = false;
    boolean isEagle = false;
    boolean isParrot = false;
    boolean isHamster = false;
    private String nama;
    private int length;
    private String kategori;

    public Animal(String nama, int length, String species, String kategori) {
        this.nama = nama;
        this.length = length;
        if(species.equals("cat")) {
            this.isCat = true;
        } else if(species.equals("eagle")) {
            this.isEagle = true;
        } else if(species.equals("parrot")) {
            this.isParrot = true;
        } else if(species.equals("hamster")) {
            this.isHamster = true;
        } else if(species.equals("lion")) {
            this.isLion = true;
        }
        this.kategori = kategori;
    }
    
    public void doSomething(int command, String kata) {
        if (isParrot) {
            if (command==2){
                System.out.println(nama+" says:"+kata.toUpperCase());
                System.out.println("Back to the office!");
            }
        }
    }
    public void doSomething(int command) {
        if (isCat){
            if (command==1){
                System.out.println("Time to clean "+nama+"\'s fur");
                System.out.println(nama+" makes a voice: Nyaaan..");
                System.out.println("Back to the office!");
            } else if (command==2){
                int x = random.nextInt(3);
                String voice ="";
                if (x==0) voice = "Miaaaw..";
                else if (x==1) voice = "Purrr..";
                else if (x==2) voice = "Mwaw!";
                else voice = "Mraaawr!";
                System.out.println(nama+" makes a voice: "+voice);
                System.out.println("Back to the office!");
            } else{
                System.out.println("You do nothing!");
                System.out.println("Back to the office!");
            }
        }
        else if (isEagle) {
            if (command==1){
                System.out.println(nama+" makes a voice: kwaakk...");
                System.out.println("You hurt!");
                System.out.println("Back to the office!");
            } else {
                System.out.println("You do nothing!");
                System.out.println("Back to the office!");
            }
        }
        else if (isHamster) {
            if (command==1) {
                System.out.println(nama+" makes a voice: ngkrrit...ngkriiitt");
                System.out.println("Back to the office!");
            } else if (command==2) {
                System.out.println(nama+" makes a voice: trrr...trrr");
                System.out.println("Back to the office!");
            } else {
                System.out.println("You do nothing!");
                System.out.println("Back to the office!");
            }
        }
        else if (isParrot) {
            if (command==1) {
                System.out.println("Parrot "+nama+" flies!");
                System.out.println(nama+" makes a voice:FLYYYYYY!!");
                System.out.println("Back to the office!");
            } else {
                System.out.println(nama+" says: HM?");
                System.out.println("Back to the office!");
            }
        }
        else if (isLion) {
            if (command==1){
                System.out.println("Lion is hunting..");
                System.out.println(nama+" makes a voice: err!!");
                System.out.println("Back to the office!");
            } else if (command==2){
                System.out.println("Clean the lion's mane..");
                System.out.println(nama+" makes a voice:Hauhhmm!");
                System.out.println("Back to the office!");
            } else if (command==3){
                System.out.println(nama+" makes a voice:HAUHHMMM!");
                System.out.println("Back to the office!");
            }
        }
    }
    
    public String getNama() {
        return this.nama;
    }
    public int getLength() {
        return this.length;
    }
    public String getKategori() {
        return this.kategori;
    }
}