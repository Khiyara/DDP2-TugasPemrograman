public class Cage {
    public static final int INDOOR_A = 45;
    public static final int INDOOR_B = 60;
    public static final int OUTDOOR_A = 75;
    public static final int OUTDOOR_B = 90;
    Animal animals;
    char level;
    
    public Cage(Animal animals) {
        this.animals = animals;
        if (animals.getKategori().equals("INDOOR")){
             if(this.animals.getLength() < INDOOR_A) {
                this.level = 'A';
            } else if(this.animals.getLength() >= INDOOR_A && this.animals.getLength() < INDOOR_B) {
                this.level = 'B';
            } else {
                this.level = 'C';
            } 
        }
        else {
             if(this.animals.getLength() < OUTDOOR_A) {
                this.level = 'A';
            } else if(this.animals.getLength() >= OUTDOOR_A && this.animals.getLength() < OUTDOOR_B) {
                this.level = 'B';
            } else {
                this.level = 'C';
            }
        }        
    }   
}